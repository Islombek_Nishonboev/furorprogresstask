package com.android.furorprogresstask.ui.put

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface PutView : MvpView {
    fun isUpdated(massage: String)
}