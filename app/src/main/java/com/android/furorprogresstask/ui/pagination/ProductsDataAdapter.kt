package com.android.furorprogresstask.ui.pagination

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.furorprogresstask.R
import com.android.furorprogresstask.data.network.response.ProductResponse
import com.android.furorprogresstask.databinding.ItemMainBinding

class ProductsDataAdapter :
    PagingDataAdapter<ProductResponse, ProductsDataAdapter.ViewHolder>(ArticleDiffItemCallback) {

    lateinit var mListener: OnItemListenerListener

    fun setListener(listener: OnItemListenerListener) {
        mListener = listener
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemMainBinding.bind(itemView)

        fun initView(item: ProductResponse?) {
            if (item != null) {
                binding.nameItem.text = item.nameUz
                binding.addressItem.text = item.address
                binding.idItem.text = item.id.toString()
                binding.costItem.text = item.cost.toString()
            }

            binding.deleteItem.setOnClickListener { mListener.onItemDeleted(item) }
            itemView.setOnClickListener { mListener.onItemClicked(item) }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.initView(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_main, parent, false)
        return ViewHolder(view)
    }

    private object ArticleDiffItemCallback : DiffUtil.ItemCallback<ProductResponse>() {

        override fun areItemsTheSame(oldItem: ProductResponse, newItem: ProductResponse): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(
            oldItem: ProductResponse,
            newItem: ProductResponse
        ): Boolean {
            return oldItem.id == newItem.id
        }
    }

    interface OnItemListenerListener {
        fun onItemDeleted(item: ProductResponse?)
        fun onItemClicked(item: ProductResponse?)
    }

}