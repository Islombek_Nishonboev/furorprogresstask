package com.android.furorprogresstask.ui.post

import com.android.furorprogresstask.data.AppConstants
import com.android.furorprogresstask.data.network.ApiService
import com.android.furorprogresstask.data.network.model.PostModel
import com.android.furorprogresstask.data.network.response.ProductResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import moxy.InjectViewState
import moxy.MvpPresenter
import org.koin.core.component.KoinComponent

@InjectViewState
class PostPresenter(private val api: ApiService) : MvpPresenter<PostView>(), KoinComponent {

    private var scope = CoroutineScope(
        Job() + Dispatchers.Main
    )

    fun createProduct(
        product_type_id: String,
        name_uz: String,
        cost: String,
        address: String,
    ) {
        if (api.isNetworkConnected()) {
        scope.launch {
            api.createPost(PostModel(product_type_id.toLong(), name_uz, cost.toLong(), address, System.currentTimeMillis()))
            viewState.isCreated(AppConstants.SUCCESS) }
        }else viewState.isCreated(AppConstants.ERROR)
    }

}