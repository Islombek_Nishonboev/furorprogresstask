package com.android.furorprogresstask.ui.put

import android.os.Bundle
import android.widget.Toast
import com.android.furorprogresstask.R
import com.android.furorprogresstask.data.AppConstants
import com.android.furorprogresstask.data.network.response.ProductResponse
import com.android.furorprogresstask.databinding.ActivityPutBinding
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get

class PutActivity : MvpAppCompatActivity(), PutView {

    @InjectPresenter
    lateinit var mPresenter: PutPresenter

    @ProvidePresenter
    fun provide(): PutPresenter = get()

    private lateinit var binding: ActivityPutBinding
    lateinit var mItem: ProductResponse


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
    }

    private fun initView() {
        mItem = intent.getSerializableExtra(AppConstants.PUT_ACTIVITY) as ProductResponse
        binding.idPut.text = mItem.id.toString()
        binding.typeIdPut.setText(mItem.productTypeId.toString())
        binding.namePut.setText(mItem.nameUz)
        binding.costPut.setText(mItem.cost.toString())
        binding.addressPut.setText(mItem.address)

        binding.btnPut.setOnClickListener { updateProduct() }
        binding.backPressedPut.setOnClickListener { onBackPressed() }

    }

    private fun updateProduct() {
        if (mItem.id.toString().isNotEmpty() &&
            binding.typeIdPut.text.toString().isNotEmpty() &&
            binding.namePut.text.toString().isNotEmpty() &&
            binding.costPut.text.toString().isNotEmpty() &&
            binding.addressPut.text.toString().isNotEmpty()
        ) {
            mPresenter.updateProduct(
                mItem.id.toString(),
                binding.typeIdPut.text.toString(),
                binding.namePut.text.toString(),
                binding.costPut.text.toString(),
                binding.addressPut.text.toString()
            )
        }
        else Toast.makeText(this, resources.getString(R.string.text_fill_error), Toast.LENGTH_SHORT).show()
    }

    override fun isUpdated(massage: String) {
        Toast.makeText(this, massage, Toast.LENGTH_SHORT).show()
    }
}