package com.android.furorprogresstask.ui.put

import com.android.furorprogresstask.data.AppConstants
import com.android.furorprogresstask.data.network.ApiService
import com.android.furorprogresstask.data.network.model.PostModel
import com.android.furorprogresstask.data.network.model.PutModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import moxy.InjectViewState
import moxy.MvpPresenter
import org.koin.core.component.KoinComponent

@InjectViewState
class PutPresenter(private val api: ApiService) : MvpPresenter<PutView>(), KoinComponent {

    private var scope = CoroutineScope(
        Job() + Dispatchers.Main
    )

    fun updateProduct(
        id: String, product_type_id: String,
        name_uz: String,
        cost: String,
        address: String,
    ) {
        if (api.isNetworkConnected()) {
            scope.launch {
                api.updateProduct(
                    PutModel(
                        id.toLong(),
                        product_type_id.toLong(), name_uz, cost.toLong(),
                        address, System.currentTimeMillis()
                    )
                )
                viewState.isUpdated(AppConstants.SUCCESS)
            }
        }
        else viewState.isUpdated(AppConstants.ERROR)

    }

}