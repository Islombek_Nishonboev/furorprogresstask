package com.android.furorprogresstask.ui.post

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import com.android.furorprogresstask.R
import com.android.furorprogresstask.databinding.ActivityPostBinding
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get

class PostActivity : MvpAppCompatActivity(), PostView {

    @InjectPresenter
    lateinit var mPresenter: PostPresenter

    @ProvidePresenter
    fun provide(): PostPresenter = get()

    private lateinit var binding: ActivityPostBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPostBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        binding.backPressed.setOnClickListener { onBackPressed() }

        binding.createdDatePost.text =
            String.format(resources.getString(R.string.text_created_date)) + System.currentTimeMillis()
                .toString()

        binding.btnPost.setOnClickListener {
            createPost()
        }
    }

    override fun isCreated(massage: String) {
        Toast.makeText(this, massage, Toast.LENGTH_SHORT).show()
    }

    private fun createPost() {
        if (
            binding.typeIdPost.text.toString().isNotEmpty() &&
            binding.namePost.text.toString().isNotEmpty() &&
            binding.costPost.text.toString().isNotEmpty() &&
            binding.addressPost.text.toString().isNotEmpty()
        ){
        mPresenter.createProduct(
            binding.typeIdPost.text.toString(),
            binding.namePost.text.toString(),
            binding.costPost.text.toString(),
            binding.addressPost.text.toString()
        )}
        else Toast.makeText(this, resources.getString(R.string.text_fill_error), Toast.LENGTH_SHORT).show()
    }
}