package com.android.furorprogresstask.ui.post

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface PostView : MvpView {

    fun isCreated(massage: String)

}