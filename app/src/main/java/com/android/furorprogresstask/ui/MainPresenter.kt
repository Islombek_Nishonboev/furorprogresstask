package com.android.furorprogresstask.ui

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.android.furorprogresstask.data.AppConstants
import com.android.furorprogresstask.data.network.ApiService
import com.android.furorprogresstask.ui.pagination.ProductsPagingSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import moxy.InjectViewState
import moxy.MvpPresenter
import org.koin.core.component.KoinComponent

@InjectViewState
class MainPresenter(private val api: ApiService) : MvpPresenter<MainView>(), KoinComponent {

    private var scope = CoroutineScope(
        Job() + Dispatchers.Main
    )


    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        getProducts()
    }

    fun getProducts() {
        if (api.isNetworkConnected()) {
            viewState.hasConnection(true)
            scope.launch {
                getProductConfig().collect { viewState.getProducts(it) }
            }
        } else viewState.hasConnection(false)

    }

    fun deleteProduct(id: Int) {
        if (api.isNetworkConnected()) {
            scope.launch {
                api.deleteProduct(id)
                getProducts()
                viewState.isDeleted(AppConstants.SUCCESS)
            }
        } else {
            viewState.isDeleted(AppConstants.ERROR)
        }
    }

    private fun getProductConfig() = Pager(
        config = PagingConfig(
            pageSize = 5, // how many items per page
            maxSize = 20, // maximum items may be loaded into PagingData before other pages are dropped
            enablePlaceholders = false
        ),
        pagingSourceFactory = { ProductsPagingSource(api) }
    ).flow.cachedIn(scope)

}