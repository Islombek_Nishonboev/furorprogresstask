package com.android.furorprogresstask.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.furorprogresstask.data.AppConstants
import com.android.furorprogresstask.data.network.response.ProductResponse
import com.android.furorprogresstask.databinding.ActivityMainBinding
import com.android.furorprogresstask.ui.pagination.ProductsDataAdapter
import com.android.furorprogresstask.ui.post.PostActivity
import com.android.furorprogresstask.ui.put.PutActivity
import kotlinx.coroutines.launch
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get


class MainActivity : MvpAppCompatActivity(), MainView, ProductsDataAdapter.OnItemListenerListener {

    @InjectPresenter
    lateinit var mPresenter: MainPresenter

    @ProvidePresenter
    fun provide(): MainPresenter = get()

    private lateinit var binding: ActivityMainBinding
    private lateinit var mAdapter: ProductsDataAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        initRecycler()
    }

    private fun initView() {

        binding.btnCreate.setOnClickListener {
            startActivity(
                Intent(
                    this, PostActivity::class.java
                )
            )
        }
    }

    private fun initRecycler() {
        mAdapter = ProductsDataAdapter()
        binding.recyclerMain.layoutManager = LinearLayoutManager(this)
        binding.recyclerMain.adapter = mAdapter

        mAdapter.setListener(this)

        binding.btnRefresh.setOnClickListener { mPresenter.getProducts() }

    }

    override fun getProducts(products: PagingData<ProductResponse>) {
        lifecycleScope.launch { mAdapter.submitData(products) }
    }

    override fun isDeleted(massage: String) {
        Toast.makeText(this, massage, Toast.LENGTH_SHORT).show()
    }

    override fun hasConnection(isConnected: Boolean) {
        if (isConnected) {
            binding.recyclerMain.visibility = View.VISIBLE
            binding.containerError.visibility = View.GONE
        } else {
            binding.recyclerMain.visibility = View.GONE
            binding.containerError.visibility = View.VISIBLE
        }
    }

    override fun onItemDeleted(item: ProductResponse?) {
        if (item != null) {
            mPresenter.deleteProduct(item.id!!.toInt())
        }
    }

    override fun onItemClicked(item: ProductResponse?) {
        val intent = Intent(this, PutActivity::class.java)
        intent.putExtra(AppConstants.PUT_ACTIVITY, item)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        mPresenter.getProducts()
    }
}