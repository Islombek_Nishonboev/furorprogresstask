package com.android.furorprogresstask.ui

import androidx.paging.PagingData
import com.android.furorprogresstask.data.network.response.ProductResponse
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface MainView : MvpView {

    fun getProducts(products: PagingData<ProductResponse>)
    fun isDeleted(massage: String)
    fun hasConnection(isConnected: Boolean)
}