package com.android.furorprogresstask.di

import com.android.furorprogresstask.di.module.networkModule
import com.android.furorprogresstask.di.module.presentationModule

val appComponent = listOf(networkModule, presentationModule)
