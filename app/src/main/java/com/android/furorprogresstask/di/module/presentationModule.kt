package com.android.furorprogresstask.di.module

import com.android.furorprogresstask.ui.MainPresenter
import com.android.furorprogresstask.ui.post.PostPresenter
import com.android.furorprogresstask.ui.put.PutPresenter
import org.koin.dsl.module

val presentationModule = module {
    factory { MainPresenter(get()) }
    factory { PostPresenter(get()) }
    factory { PutPresenter(get()) }
}