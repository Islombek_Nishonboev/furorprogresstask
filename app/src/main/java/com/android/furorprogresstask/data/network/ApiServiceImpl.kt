package com.android.furorprogresstask.data.network

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.android.furorprogresstask.data.network.model.PostModel
import com.android.furorprogresstask.data.network.model.PutModel
import com.android.furorprogresstask.data.network.response.ProductResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class ApiServiceImpl(private val context: Context, private val api: Api) : ApiService {


    override fun isNetworkConnected(): Boolean {
        var result = false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val actNw =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }
                }
            }
        }
        return result
    }


    override suspend fun getProducts(page: Int, perPage: Int): Response<List<ProductResponse>> {
        return withContext(Dispatchers.IO) {
            return@withContext api.getProducts(page, perPage)
        }
    }

    override suspend fun deleteProduct(id: Int) {
        return withContext(Dispatchers.IO) {
            return@withContext api.deleteProduct(id)
        }
    }

    override suspend fun createPost(postModel: PostModel) {
        return withContext(Dispatchers.IO) {
            return@withContext api.createProduct(postModel)
        }
    }

    override suspend fun updateProduct(putModel: PutModel) {
        return withContext(Dispatchers.IO) {
            return@withContext api.updateProduct(putModel)
        }
    }
}