package com.android.furorprogresstask.data.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable


@JsonClass(generateAdapter = true)
class ProductResponse : Serializable {

    @Json(name = "id")
    var id: Long? = null

    @Json(name = "product_type_id")
    var productTypeId: Long? = null

    @Json(name = "address")
    var address: String? = null


    @Json(name = "name_uz")
    var nameUz: String? = null

    @Json(name = "cost")
    var cost: Long? = null

    @Json(name = "created_date")
    var createdDate: Long? = null

}