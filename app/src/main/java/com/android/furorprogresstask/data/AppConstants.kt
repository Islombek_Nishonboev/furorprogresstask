package com.android.furorprogresstask.data

interface AppConstants {
    companion object {
        const val PUT_ACTIVITY = "PUT_ACTIVITY"
        const val SUCCESS = "Success"
        const val ERROR = "Error"
    }
}