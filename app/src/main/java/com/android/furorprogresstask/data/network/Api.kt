package com.android.furorprogresstask.data.network

import com.android.furorprogresstask.data.network.model.PostModel
import com.android.furorprogresstask.data.network.model.PutModel
import com.android.furorprogresstask.data.network.response.ProductResponse
import retrofit2.Response
import retrofit2.http.*

interface Api {

    companion object {
        var BASE_URL = "http://94.158.54.194:9092/api/"
    }

    @GET("product")
    suspend fun getProducts(
        @Query("page") queryPage: Int,
        @Query("perPage") queryPerPage: Int
    ): Response<List<ProductResponse>>

    @DELETE("product/{id}")
    suspend fun deleteProduct(@Path("id") id: Int)


    @POST("product")
    suspend fun createProduct(@Body postModel: PostModel)

    @PUT("product")
    suspend fun updateProduct(@Body putModel: PutModel)
}