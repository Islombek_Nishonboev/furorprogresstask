package com.android.furorprogresstask.data.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
class PutModel(

    @Json(name = "id")
    var id: Long,

    @Json(name = "product_type_id")
    var product_type_id: Long,

    @Json(name = "name_uz")
    var name_uz: String,

    @Json(name = "cost")
    var cost: Long,

    @Json(name = "address")
    var address: String,

    @Json(name = "created_date")
    var created_date: Long
)
