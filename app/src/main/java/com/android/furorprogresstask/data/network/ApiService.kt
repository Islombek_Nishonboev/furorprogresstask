package com.android.furorprogresstask.data.network

import com.android.furorprogresstask.data.network.model.PostModel
import com.android.furorprogresstask.data.network.model.PutModel
import com.android.furorprogresstask.data.network.response.ProductResponse
import retrofit2.Response
import retrofit2.http.Field

interface ApiService {

    fun isNetworkConnected(): Boolean

    suspend fun getProducts(page: Int, perPage: Int): Response<List<ProductResponse>>
    suspend fun deleteProduct(id: Int)
    suspend fun createPost(postModel: PostModel)
    suspend fun updateProduct(putModel: PutModel)

}